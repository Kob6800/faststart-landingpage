=== Faststart Landingpage ===

== Description ==

This plugin is based on a generated Boilerplate from [wppb.me](https://wppb.me/).

The plugin registers two shortcodes to the wordpress setup.
These can then be used within a theme to add content blocks to a page.
There is a more classic, rational one and a more playful one:

* [fs_initRational targetid='targetForm']
* [fs_initPlayful targetid='targetForm']

The parameter *targetid* is currently not in use, but is intended to link anchor links within the generated content 
to a form where user can register for testdrives.

== Insert landingpage content ==

Insert content block via shortcode on theme side (for example in index.php) like: 

`<?php if (shortcode_exists('fs_initRational')) {
     $contentBlock = do_shortcode("[fs_initRational targetid='targetForm']");
     echo '<div>'. $contentBlock. '</div>' ;
 } ?>`
     
`<?php if (shortcode_exists('fs_initPlayful')) {
     $contentBlock2 = do_shortcode("[fs_initPlayful targetid='targetForm']");//"rational", "targetForm");
     echo '<div>'. $contentBlock2. '</div>' ;
 } ?>`

An example can be found at BMW-Testsite repo, which is used as testing Mockup.

== Architecture overview / Important files ==

Because main purpose of plugin is to provide 2 shortcodes, most of the functionality can be found in the pulbic dir.

* entrypoint for setup ./includes/class-faststart-landingpage.php
* entry point for shortcodes ./public/class-faststart-landingpage-public.php
* main logic ./public/js/faststartLandingPage.js
* Custom Sass code used for generated content integrated into provided style.scss with an fs {} encapsulation
* used new font bmw_global_pro (see _bmwfonts.scss)

== Dependencies (shiped with plugin) ==
* Bootstrap 3.3.7
* JQuery 3.3.1
* fonts

== TODO == 
* cleanup
* garage feature for playful variant
* add tracking code

Optional
* rework resource strings to tempaltes/partials