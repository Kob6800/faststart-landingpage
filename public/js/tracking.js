var _paq = _paq || [];
/* tracker methods like "setCustomDimension" should be called before "trackPageView" */
// _paq.push(['trackPageView']);
// _paq.push(['enableLinkTracking']);
(function() {
    //_paq will be init outside of plugin
    if( typeof _paq === 'undefined' ) {
        //local
        // var u="//localhost:81/matomo/";
        // _paq.push(['setTrackerUrl', u+'piwik.php']);
        // _paq.push(['setSiteId', '5']);
        //live test srv
        //call from http://bmw-musterhaus1-ort1.web01.adwerba.at
        // var u="//tracking.bmw.at/";
        // _paq.push(['setTrackerUrl', u+'piwik.php']);
        // _paq.push(['setSiteId', '396']);

        // _paq.push(['setGenerationTimeMs', 0]);
        // _paq.push(['trackPageView']);

        // _paq.push(['trackAllContentImpressions']);
        // _paq.push(['trackVisibleContentImpressions']);

        // _paq.push(['enableLinkTracking']);

        // var d=document, g=d.createElement('script'),
        //     s=d.getElementsByTagName('script')[0];
        // g.type='text/javascript';
        // g.async=true; g.defer=true;
        // g.src=u+'piwik.js';
        // s.parentNode.insertBefore(g,s);
        return;
    }
    _paq.push(['trackPageView']);
    _paq.push(['trackVisibleContentImpressions']);
    _paq.push(['enableLinkTracking']);

})();


// var currentUrl = location.href;
// _paq.push(['setReferrerUrl', currentUrl]);
// currentUrl = '' + window.location.hash.substr(1);
// _paq.push(['setCustomUrl', currentUrl]);
// _paq.push(['setDocumentTitle', 'My New Title']);
// remove all previously assigned custom variables, requires Matomo (formerly Piwik) 3.0.2
// _paq.push(['deleteCustomVariables', 'page']);


<!-- End Matomo Code -->

// <!-- Matomo proxy-->
// <script type="text/javascript">
// var _paq = _paq || [];
// /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
// _paq.push(["setCookieDomain", "*.localhost"]);
// _paq.push(["setDomains", ["*.localhost/wordpress"]]);
// _paq.push(['trackPageView']);
// _paq.push(['trackVisibleContentImpressions']);
// _paq.push(['enableLinkTracking']);
// (function() {
//     var u="//localhost/wordpress/wp-content/plugins/wp-piwik/proxy/"
//     _paq.push(['setTrackerUrl', u+'piwik.php']);
//     _paq.push(['setSiteId', '3']);
//     var d=document, g=d.createElement('script'),
//         s=d.getElementsByTagName('script')[0];
//     g.type='text/javascript';
//     g.async=true;g.defer=true;
//     g.src=u+'piwik.php';
//     s.parentNode.insertBefore(g,s);
// })();
// </script>
// <!-- End Matomo Code -->
//
// <!-- Matomo js/index.php-->
// <script type="text/javascript">
// var _paq = _paq || [];
// /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
// _paq.push(["setCookieDomain", "*.localhost"]);
// _paq.push(["setDomains", ["*.localhost/wordpress"]]);
// _paq.push(['trackPageView']);
// _paq.push(['trackVisibleContentImpressions']);
// _paq.push(['enableLinkTracking']);
// (function() {
//     var u="//localhost:81/matomo/";
//     _paq.push(['setTrackerUrl', u+'piwik.php']);
//     _paq.push(['setSiteId', '3']);
//     var d=document, g=d.createElement('script'),
//         s=d.getElementsByTagName('script')[0];
//     g.type='text/javascript';
//     g.async=true;g.defer=true;
//     g.src=u+'piwik.js';
//     s.parentNode.insertBefore(g,s);
// })();
// </script>
// <!-- End Matomo Code -->
//
// <!-- Matomo  default-->
// <script type="text/javascript">
// var _paq = _paq || [];
// /* tracker methods like "setCustomDimension" should be called before "trackPageView" */
// _paq.push(['trackPageView']);
// _paq.push(['trackVisibleContentImpressions']);
// _paq.push(['enableLinkTracking']);
// (function() {
//     var u="//localhost:81/matomo/";
//     _paq.push(['setTrackerUrl', u+'js/index.php']);
//     _paq.push(['setSiteId', 'n/a']);
//     var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
//     g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'js/index.php'; s.parentNode.insertBefore(g,s);
// })();
// </script>
// <!-- End Matomo Code -->
