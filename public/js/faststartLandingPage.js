(function ($) {
    'use strict';

    let fsRationalContainer;
    let fsPlayfulContainer;
    $(window).load(function () {
        //console.log("hello on window load from JS");
        // _paq.push(['logAllContentBlocksOnPage']);
        setupJS();
        fsRationalContainer = document.getElementById('fs-wrapper-rational');
        fsPlayfulContainer = document.getElementById('fs-wrapper-playful');
        if (fsRationalContainer) {
            renderAccordionView();
        }
        if (fsPlayfulContainer) {
            renderParallaxView();
        }
        restyleFeatureView2WinSize();
        // _paq.push(['logAllContentBlocksOnPage']);
        // _paq.push(['trackContentImpressionsWithinNode', document]);
    });

})(jQuery);

function addEvent(object, type, callback) {
    if (object == null || typeof(object) == 'undefined') return;
    if (object.addEventListener) {
        object.addEventListener(type, callback, false);
    } else if (object.attachEvent) {
        object.attachEvent("on" + type, callback);
    } else {
        object["on" + type] = callback;
    }
};
addEvent(window, "resize", function (event) {
    restyleFeatureView2WinSize();
    restyleParallax();
});

//--------------------------------------------------------------------------------
// CONSTANTS
const PUBLIC = faststartLandingPage.pluginUrl;
const IMAGEPATH = PUBLIC + 'resources/images/';
// console.log(PUBLIC);
// console.log(IMAGEPATH);
const PARALAXMOVEMENT = 0.01;
const FORMANCHOR = "#rfo";

const X1HEADER = 'Der BMW&nbsp;X1.';
const X1TEXT = 'Der BMW&nbsp;X1 ist der perfekte Begleiter für alle, die jeden Tag neue Herausforderungen suchen. ' +
    'Mit Business Line überzeugt der BMW X1 jetzt mit einem Preisvorteil von bis zu <strong>€&nbsp4.300,-*</strong>. ' +
    'Entdecken Sie die Features die inbegriffen sind.';
const X2HEADER = 'Der BMW&nbsp;X2.';
const X2TEXT = 'Absolut einzigartig. Extrem außergewöhnlich.' +
    'Der erste BMW&nbsp;X2 bietet mit seiner kühnen Athletik eine dynamische und agile Performance, die in dieser Klasse ihresgleichen sucht. ' +
    'Mit Business Line überzeugt der BMW X2 jetzt mit einem Preisvorteil von bis zu <strong> €&nbsp4.300,-* </strong> und innovativen Features.';
const X3HEADER = 'Der BMW&nbsp;X3.';
const X3TEXT = 'Der neue BMW&nbsp;X3 ist ein Statement unbegrenzter Möglichkeiten und Ausdruck purer Präsenz und Freiheit. ' +
    'Dank teilautomatisiertem Fahren und dem intelligenten Allradantrieb BMW xDrive bietet er höchsten Fahrkomfort auf und abseits der Straße. ' +
    'Mit Business Line überzeugt der BMW X3 jetzt mit einem Preisvorteil von bis zu <strong> €&nbsp4.300,-* </strong> und innovativen Features.';
const NAVIHEADER = 'Navigationssystem Professional';
const NAVITEXT = 'Das Navigationssystem Professional bietet Ihnen ein einfach zu bedienendes, optimal in Ihren BMW ' +
    'integriertes Navigationssystem inklusive iDrive Bedienkonzept. ' +
    'Die Zielführung erfolgt im 8,8\'\' hochauflösendem Farbbildschirm und kann per ' +
    '<strong>Sprachbefehl</strong> gesteuert werden. Zusätzlich werden Ihnen relevante Infos ' +
    'über das <strong>BMW Head-Up Display</strong> in die Windschutzscheibe projiziert. ';
const TELEFONIEHEADER = 'Telefonie mit Wireless Charging';
const TELEFONIETEXT = 'Dank Telefonie mit Wireless Charging können Sie Ihr Smartphone via Bluetooth mit Ihrem Fahrzeug ' +
    'verbinden und gleichzeitig kabellos aufladen. Zusätzlich beinhaltet dieses Feature ' +
    'die <strong>WLAN Hotspot Vorbereitung</strong>.';
const KAMERAHEADER = 'Rückfahrkamera';
const KAMERATEXT = 'Die Rückfahrkamera zeigt den Bereich hinter dem Fahrzeug während des Ein- und Ausparkens an. ' +
    'Optional wird der Fahrer durch zusätzliche Parkhilfslinien und Hindernismarkierungen unterstützt.';
const PDCHEADER = 'Parksensoren vorne und hinten';
const PDCTEXT = 'Die Parksensoren vorne und hinten, erleichtert Ihnen das Einparken und Rangieren auf engstem Raum. ' +
    'Signaltöne sowie eine optische Abstandsanzeige im Control Display' +
    '(Grün/Gelb/Rot) melden Ihnen eine Annäherung an Objekte hinter bzw. vor Ihrem Fahrzeug.';
const RTTIHEADER = 'Echtzeit-Verkehrs&shy;informationen';
const RTTITEXT = 'Die Echtzeit-Verkehrs&shy;informationen haben das aktuelle Verkehrsgeschehen immer für Sie im Blick. ' +
    'Zu Staus auf dem Weg erhalten Sie frühzeitig und automatisch Vorschläge zu zeitsparenden Ausweichrouten. ' +
    'Die Informationen über Probleme auf Ihrer geplanten Route werden dabei nahezu in <strong>Echtzeit</strong> erfasst und sind bis auf hundert Meter genau.';
const SITZHEIZUNGHEADER = 'should not be visible';
const SITZHEIZUNGTEXT = 'should not be visible';

let TrackingFeatureViewCar="";
let fsRationalContentElement;
let fsPlayfulContentElement;
let fsPlayfulCache;
function setupJS() {
    fsRationalContentElement = document.getElementById('fs-content-rational');
    fsPlayfulContentElement = document.getElementById('fs-content-playful');
    fsPlayfulCache = document.getElementById('fs-cache-playful');
}


//--------------------------------------------------------------------------------
// SHARED
//--------------------------------------------------------------------------------

//create CARouselBlock wit id stringID and append it to fsContentElement
function renderCARouselBlock(fsContentElement) {

    if(fsContentElement.id === "fs-content-rational") {
        let id = 'nonSpotlightCarousel-rational';
        let variant = 'Rational';
        //if this element had been loaded before display it ...
        let block = document.getElementById(id);
        if (block) {
            // console.log("display raitonal");
            fsContentElement.appendChild(block);
        }
        //else create it
        else {
            var tmp = document.createElement('div');
            tmp.setAttribute('id', id);
            tmp.innerHTML = createCarousel(variant);
            fsContentElement.appendChild(tmp);
        }
        registerCarouselListener(variant);

        // _paq.push(['trackContentImpressionsWithinNode', document]);
    } else if (fsContentElement.id === "fs-content-playful") {
        let id = 'nonSpotlightCarousel-playful';
        let variant = 'Playful';
        //if this element had been loaded before display it ...
        let block = document.getElementById(id);
        if (block) {
            // console.log("display playful");
            fsContentElement.appendChild(block);
        }
        //else create it
        else {
            var tmp = document.createElement('div');
            tmp.setAttribute('id', id);
            tmp.innerHTML = createCarousel(variant);
            fsContentElement.appendChild(tmp);
        }
        registerCarouselListener(variant);
    }
    else {
        console.log("renderCARousel: unkown contentElement given");
    }
    //_paq.push(["trackContentImpressionsWithinNode", documentOrElement]);
}

//register textupdate on image change in carousel
function registerCarouselListener(variant) {
    jQuery(function ($) {
        $('#nonSpotlightCars-'+variant).bind('slide.bs.carousel', function (e) {
            // console.log('slide event! ' + variant);
            let nextId = $(e.relatedTarget).children('h3')[0].id;
            // console.log(nextId);
            // console.log($(e.relatedTarget.previousElementSibling).children('h3')[0].id); -> null on edges
            $('#nonSpotlightCarsHeader-'+variant).text($(e.relatedTarget).children('h2').text());
            $('#nonSpotlightCarsSubHeader-'+variant).text($(e.relatedTarget).children('h3').text());
            $('#nonSpotlightCarsBody-'+variant).html($(e.relatedTarget).children('p').html());
            $('#nonSpotlightCarsBtnWrapper-'+variant).children().addClass('fs-carousel-inv');
            $('#nonSpotlightCarsBtn'+nextId+'-'+variant).removeClass('fs-carousel-inv');

        });
    });
}

function scroll2TargetForm(car, variant, callElement) {
    // console.log("hello from scroll " + car);
    // console.log("callE: " + callElement);
    // console.log("variant: " + variant);
    // console.log("car: " + car);
    let value="F48-GF";
    switch (car) {
        case "X1":
            value = "F48-GF";
            break;
        case "X2":
            value = "F39-SC";
            break;
        case "X3":
            value = "G01-GF";
            break;
        case "3T":
            value = "F31-TO";
            break;
        case "3GT":
            value = "F34-ST";
            break;
        case "4GC":
            value = "F32-CP";
            break;
        case "5L":
            value = "G30-LI";
            break;
        case "5T":
            value = "G31-TO";
            break;
        case "X6":
            value = "F16-SC";
            break;
        case "6GT":
            value = "G32-ST";
            break;
        case "X4":
            value = "G02-SC";
            break;

        default:
            break;
    }
    jQuery(function ($) {
        $('.form-control option[value="'+value+'"]').prop('selected', true);
        var offset = $('#rfo').offset();
        offset.top -= 50;
        $('html, body').animate({
            scrollTop: offset.top
        }, "slow");
    });

    _paq.push(['trackEvent', variant, callElement, "RFO_"+car]);

}

//--------------------------------------------------------------------------------
// RATIONAL
//--------------------------------------------------------------------------------

//render accordion for X1, X2, X3 and non spotlight CARousel
function renderAccordionView() {
    // clearContent(fsContentElement, fsPlayfulCache);
    if (fsRationalContentElement) {
        var tmp = document.createElement("div");
        tmp.setAttribute("id", "fsAccordionView");
        tmp.innerHTML += createAcc1();
        tmp.innerHTML += createAcc2();
        tmp.innerHTML += createAcc3();
        fsRationalContentElement.appendChild(tmp);
        renderCARouselBlock(fsRationalContentElement);
        _paq.push(['trackContentImpressionsWithinNode', document]);
        // _paq.push(['logAllContentBlocksOnPage']);
    }
    else {
        console.log("no rational render context");
    }
}

//change image of accordeon.
// @actualChild: id of sub-header clicked,
// @pictureContainerId: id of parentelement containig all images
// @pictureFrameID: id where to display img
function accChangeImg(actualChildId, pictureContainerId, pictureFrameID) {
    
    let TAccordion;
    if(actualChildId.indexOf("X1")>=0 ){
            TAccordion = "Accordion_X1";
        }else if(actualChildId.indexOf("X2")>=0){
            TAccordion = "Accordion_X2";
        }else{
            TAccordion = "Accordion_X3";
    }
    jQuery(function ($) {
        // console.log("hello from custom jqueryfunc");
        let lastChild = document.getElementById(pictureContainerId).lastChild;
        let actualChild = document.getElementById(actualChildId);

        document.getElementById(pictureContainerId).insertBefore(actualChild, lastChild);

        $('#' + pictureContainerId).children().each(function () {
            if ($(this).attr("id") !== pictureFrameID) {
                let percentageHeight = Math.round(
                    $(this).height() /
                    $(this).parent().height() * 100
                );
                if ($(this).attr("id") == actualChildId) {
                    if (percentageHeight == 100) {
                        $(this).css("height", "0%");
                    } else {
                        $(this).css("height", "100%");
                    }
                } else {
                    var id = $(this).attr("id");
                    setTimeout(function () {
                        document.getElementById(id).style.height = "0";
                    }, 700);
                }
            }
        });
    });
    _paq.push(['trackEvent', 'Rational', TAccordion, actualChildId]);
}


//--------------------------------------------------------------------------------
// PLAYFUL
//--------------------------------------------------------------------------------

//move visible content to hidden div
function clearPlayfulContent() {
    // let cnt = 0;
    while (fsPlayfulContentElement.firstChild) {
        // console.log(cnt + ' ' + fsContentElement.firstChild);
        fsPlayfulCache.appendChild(fsPlayfulContentElement.firstChild);
        // cnt++;
    }
}

//clear Content and render x1, x2, x3 parallax banners + non spotlight CARousel
function renderParallaxView() {
    if (fsPlayfulContentElement && fsPlayfulCache) {
        clearPlayfulContent();
        //if this element had been loaded before display it ...
        let block = document.getElementById("fsParallaxView");
        if (block) {
            fsPlayfulContentElement.appendChild(block);
        }
        //else create it
        else {
            var tmp = document.createElement("div");
            tmp.setAttribute("id", "fsParallaxView");
            tmp.innerHTML = createParallaxView();
            fsPlayfulContentElement.appendChild(tmp);
        }
        registerParallaxListener();
        restyleParallax();
        registerParallaxTracker();
        renderCARouselBlock(fsPlayfulContentElement);
        _paq.push(['trackContentImpressionsWithinNode', document]);
        // _paq.push(['logAllContentBlocksOnPage']);
    } else {
        console.log("no playful render context");
    }
}

function registerParallaxTracker() {
    jQuery(function ($) {
        $("#parallaxX1").mouseenter(function() {
            _paq.push(['trackEvent', 'Playful', 'Parallax_X1', 'MouseOver']);
        });
        $("#parallaxX2").mouseenter(function() {
            _paq.push(['trackEvent', 'Playful', 'Parallax_X2', 'MouseOver']);
        });
        $("#parallaxX3").mouseenter(function() {
            _paq.push(['trackEvent', 'Playful', 'Parallax_X3', 'MouseOver']);
        });
    });
}
//register parallax fx on mousemovement
function registerParallaxListener() {
    jQuery(function ($) {
        var currentX = '';
        var currentY = '';
        $(document).mousemove(function (e) {
            if (currentX == '') {
                currentX = e.pageX;
            }
            var xdiff = e.pageX - currentX;
            currentX = e.pageX;
            if (currentY == '') {
                currentY = e.pageY;
            }
            var ydiff = e.pageY - currentY;
            currentY = e.pageY;
            $(".fs-parallax:hover img").each(function (i, el) {
                var movement = (i + 1) * (xdiff * PARALAXMOVEMENT);
                var movementy = (i + 1) * (ydiff * PARALAXMOVEMENT);
                var newX = $(el).position().left + movement;
                var newY = $(el).position().top + movementy;
                $(el).css('transition', '');
                $(el).css('left', newX + 'px');
                $(el).css('top', newY + 'px');
            });
        });
    });

}

//restyle parallax boxes
function restyleParallax() {
    //console.log("hello from restyleParallax");
    var w = window.innerWidth
        || document.documentElement.clientWidth
        || document.body.clientWidth;

    var isMobile = {
        Android: function () {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function () {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function () {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function () {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function () {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function () {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };

    let ParallaxBoxen=document.getElementsByClassName("fs fs-parallaxView fs-box");

    if (w <= 650 || isMobile.any()) {
        for (i = 0; i < ParallaxBoxen.length; i++) {
            ParallaxBoxen[i].style.height = "45vh";
        }
    }else{
        for (i = 0; i < ParallaxBoxen.length; i++) {
            ParallaxBoxen[i].style.height = "65vh";
        }
    }
}

//resets pos of child img layers of parallax container parent.
function fsResetParalax(parent) {
    // console.log('hello from reset '+$(parent));
    for (i = 0; i < parent.children.length; i++) {
        // console.log(i + ' ' + parent.children[i]);
        parent.children[i].style.top = 0;
        parent.children[i].style.left = 0;
        parent.children[i].style.transition = ".7s ease";
    }
}

//render feature view and load resources of given car.
function renderFeatureView(car) {
    if (fsPlayfulContentElement && fsPlayfulCache) {
        clearPlayfulContent();
        //if this element had been loaded before display it ...
        let block = document.getElementById("fsFeatureViewWrapper");
        if (block) {
            fsPlayfulContentElement.appendChild(block);
            // console.log('hello world1');
            jQuery(function ($) {
                // console.log($('#featureViewTargetBtn'));
                $('#featureViewTargetBtn').removeAttr('onclick');
                $('#featureViewTargetBtn').off('click');
                $('#featureViewTargetBtn').on('click', function () {
                    scroll2TargetForm(car, 'Playful' , 'FeatureView_'+car )
                    // id="featureViewTargetBtn" onclick=\'scroll2TargetForm("' + car + '","Playful","FeatureView_' + car + '");
                });
            });
        }
        //else create it
        else {
            var tmp = document.createElement("div");
            tmp.setAttribute("id", "fsFeatureViewWrapper");
            tmp.innerHTML = createFeatureView(car);
            fsPlayfulContentElement.appendChild(tmp);
        }
        changeCppAndCar(car);
        restyleFeatureView2WinSize();
        // restyleParallax();
        registerPunkteHover();
        _paq.push(['trackContentImpressionsWithinNode', document]);
        // _paq.push(['logAllContentBlocksOnPage']);
        jQuery(function ($) {
            var offset = $("#fsFeatureViewWrapper").offset();
            offset.top -= 50;
            $('html, body').animate({
                scrollTop: offset.top
            }, "slow");
        });
    } else {
        console.log("no playful render context");
    }
    TrackingFeatureViewCar="FeatureView_" + car;
    _paq.push(['trackEvent', 'Playful', 'Parallax_'+ car , 'Click']);
}

//do couple of styling stuff on resize (and load as it is called once there)
function restyleFeatureView2WinSize() {
    // console.log("hello from feature view restyle");
    if (document.getElementById("fsFeatureViewContainer")) {
        var w = window.innerWidth
            || document.documentElement.clientWidth
            || document.body.clientWidth;

        var isMobile = {
            Android: function () {
                return navigator.userAgent.match(/Android/i);
            },
            BlackBerry: function () {
                return navigator.userAgent.match(/BlackBerry/i);
            },
            iOS: function () {
                return navigator.userAgent.match(/iPhone|iPad|iPod/i);
            },
            Opera: function () {
                return navigator.userAgent.match(/Opera Mini/i);
            },
            Windows: function () {
                return navigator.userAgent.match(/IEMobile/i);
            },
            any: function () {
                return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
            }
        };

        let panel = document.getElementById("fs-colorpanel");
        let picture1 = document.getElementById("fs-cpp1");
        let picture2 = document.getElementById("fs-cpp2");
        let picture3 = document.getElementById("fs-cpp3");
        let picture4 = document.getElementById("fs-cpp4");
        let bgw = document.getElementById("bgw");
        let bgi = document.getElementById("bg-change");
        let fcbWrapper = document.getElementById("FCBWrapper");
        let fcbBlock = document.getElementById("FeatureContentBlock");
        let ddblockre = document.getElementById("ddblock3");
        let rfobtn = document.getElementsByClassName("btn btn-info fs fs-feature fs-target-btn")

        let i;
        let punkte = document.getElementsByClassName("fs-kreis");
        let j;
        let img = document.getElementsByClassName("FCBB");

        if (w <= 650 || isMobile.any()) {
            panel.classList.remove("fs-colorpanel");
            panel.className = "fs-colorpanel_quer";
            picture1.className = "fs fs feature fs-cpp_quer";
            picture2.className = "fs fs feature fs-cpp_quer";
            picture3.className = "fs fs feature fs-cpp_quer";
            picture4.className = "fs fs feature fs-cpp_quer";
            bgi.style.marginTop = "10%";
            // bgw.style.marginTop = "-5%";
            fcbWrapper.style.marginTop = "50%";
            fcbWrapper.style.width = "88%";
            fcbBlock.style.width= "95%";
            ddblockre.style.marginLeft = "-100px";
            rfobtn[0].style.bottom = "unset";
            rfobtn[0].style.top = "-20px";
            rfobtn[0].style.right = "0px";

            for (i = 0; i < punkte.length; i++) {
                punkte[i].style.height = "15px";
                punkte[i].style.width = "15px";
            }
            for (j = 0; j < img.length; j++) {
                img[j].style.width = "50vw";
            }
        }
        else {
            panel.classList.remove("fs-colorpanel_quer");
            panel.className = "fs-colorpanel";
            picture1.className = "fs fs feature fs-cpp";
            picture2.className = "fs fs feature fs-cpp";
            picture3.className = "fs fs feature fs-cpp";
            picture4.className = "fs fs feature fs-cpp";
            bgi.style.marginTop = "0";
            // bgw.style.marginTop = "-10%";
            fcbWrapper.style.marginTop = "18%";
            fcbWrapper.style.width = "52vw";
            fcbBlock.style.width= "45vw";
            ddblockre.style.marginLeft = "-15px";
            rfobtn[0].style.bottom = "10px";
            rfobtn[0].style.top = "unset";
            rfobtn[0].style.right = "150px";

            for (i = 0; i < punkte.length; i++) {
                punkte[i].style.height = "25px";
                punkte[i].style.width = "25px";
            }
            for (j = 0; j < img.length; j++) {
                img[j].style.width = "100%";
            }
        }
    }
}

//Exchange elements of feature View depending on Car selected.
//I.e. change ColorPalettePicker, background, header + text and hide alrdy shown features.
function changeCppAndCar(car) {
    let bgi = document.getElementById("bg-change");
    bgi.src = IMAGEPATH + "featureView/" + car + "/bild1.jpg";
    let picture1 = document.getElementById("fs-cpp1");
    let picture2 = document.getElementById("fs-cpp2");
    let picture3 = document.getElementById("fs-cpp3");
    let picture4 = document.getElementById("fs-cpp4");
    let header = document.getElementById("FeatureViewHeader");
    let text = document.getElementById("FeatureViewText");

    let FV="FeatureView_" + car;

    picture1.parentElement.onclick = function () {
        colorFeatureImageWithSource("bild1.jpg", car);
        _paq.push(['trackEvent', 'Playful', FV, 'ChangeColor1']);
    };
    picture2.parentElement.onclick = function () {
        colorFeatureImageWithSource("bild2.jpg", car);
        _paq.push(['trackEvent', 'Playful', FV, 'ChangeColor2']);
    };
    picture3.parentElement.onclick = function () {
        colorFeatureImageWithSource("bild3.jpg", car);
        _paq.push(['trackEvent', 'Playful', FV, 'ChangeColor3']);
    };
    picture4.parentElement.onclick = function () {
        colorFeatureImageWithSource("bild4.jpg", car);
        _paq.push(['trackEvent', 'Playful', FV, 'ChangeColor4']);
    };
    picture1.src = IMAGEPATH + "featureView/" + car + "/color1.jpg";
    picture2.src = IMAGEPATH + "featureView/" + car + "/color2.jpg";
    picture3.src = IMAGEPATH + "featureView/" + car + "/color3.jpg";
    picture4.src = IMAGEPATH + "featureView/" + car + "/color4.jpg";

    switch (car) {
        case "X1":
            header.innerHTML = X1HEADER;
            text.innerHTML = X1TEXT;
            picture1.alt = "alpinweiss";
            picture2.alt = "estoril blau metallic";
            picture3.alt = "mediterranblau metallic";
            picture4.alt = "saphirschwarz metallic";
            break;
        case "X2":
            header.innerHTML = X2HEADER;
            text.innerHTML = X2TEXT;
            picture1.alt = "galvanic gold";
            picture2.alt = "glaciersilber metallic";
            picture3.alt = "misano blau metallic";
            picture4.alt = "sunset orange metallic";
            break;
        case "X3":
            header.innerHTML = X3HEADER;
            text.innerHTML = X3TEXT;
            picture1.alt = "phytoniclbau";
            picture2.alt = "alpinweiss";
            picture3.alt = "sonnenstein metallic";
            picture4.alt = "sophistograu brillanteffekt metallic";
            break;
        default:
            break;
    }
    jQuery(function ($) {
        $('#FCBTexts').children().each(function () {
            $(this).attr('class', 'fs-FCBinvisible');

        });
        $('#FCBPictures').children().each(function () {
            $(this).attr('class', 'fs-FCBinvisible');
        });
    });
}

function colorFeatureImageWithSource(a, car) {
    var element = document.getElementById("bg-change");
    element.src = IMAGEPATH + "featureView/" + car + "/" + a;
    newelement = element.cloneNode(true);
    element.parentNode.replaceChild(newelement, element);
}

//show info about features selected in feature View
function changeFeatureImageAndText(a) {    

    jQuery(function ($) {
        var b = "b" + a;
        $('#FCBTexts').children().each(function () {
            if ($(this).attr("id") == a) {
                $(this).attr('class', 'fs-FCBDescription');

                if (isNotInViewportBot($(this), 100)){
                    var offset = $('#FeatureContentBlock').offset();
                    offset.top -= 100;
                    $('html, body').animate({
                        scrollTop: offset.top
                    });
                }
            } else {
                $(this).attr('class', 'fs-FCBinvisible');
            }
        });
        $('#FCBPictures').children().each(function () {
            if ($(this).attr("id") == b) {
                $(this).attr('class', 'fs-FCBBild');
            } else {
                $(this).attr('class', 'fs-FCBinvisible');
            }
        });
        //document.getElementById(a).scrollIntoView();
    });
    _paq.push(['trackEvent', 'Playful', TrackingFeatureViewCar, a]);
}

function isNotInViewportBot(element, offset) {
    var visibleLine = jQuery(window).scrollTop() + window.innerHeight;
    // console.log(element.offset().top); //abs pos of el
    // console.log(visibleLine); //bot line of visible viewport
    return element.offset().top  > visibleLine - offset;
}

//set z-vlaue for the current dropdown field in featerView infoPoints
function registerPunkteHover() {
    jQuery(function ($) {
        $('.fs-kreis').hover(function (e) {
            $('.fs-kreis').each(function (i, el) {
               $(el).css('z-index', '0');
            });
            $(e.currentTarget).css('z-index', '1');
        }, function () {
            $('.fs-kreis').each(function (i, el) {
                $(el).css('z-index', '1');
            });
        });
    });
}

function trackingAdvanced(variant) {
    var d = document;
    

}


//----------------------------------------------------------------------------------
// RES STRING HELPERS

function createFeatureView(car) {
    let $resString =
    '<div id="fsFeatureViewContainer"class="fs fs-feature fs-clearfix" data-track-content data-content-name="FeatureView'+car+'" data-content-target="'+PUBLIC+'">' +
    '<div class="fs fs-feature fs-box " >' +
        '    <div id=bgw class="fs fs-feature fs-feature-wrapper">' +
        '' +
        '        <div class="fs fs-feature fs-punkterahmen">' +
        '            <div id="rahmenzwei" data-track-content data-content-name="FeatureView'+car+'" data-content-piece="FeatureViewInfoPoints">' +
        '                 <div id="fpunkt1" class="fs fs-feature fs-kreis">' +
        '                    <div class="fs fs-feature fs-dropdown">    ' +
        '                    <div class="fs fs-feature fs-dropdown-content" id="ddblock1">' +
        '                       <a onclick=\'changeFeatureImageAndText("FCBnavi");\'>Navigationssystem Professional</a>' +
        '                       <a onclick=\'changeFeatureImageAndText("FCBtelefonie");\'>Telefonie mit Wireless Charging</a>' +
        '                       <a onclick=\'changeFeatureImageAndText("FCBrtti");\'>Echtzeit-Verkehrs&shy;informationen</a>' +
        '                    </div>' +
        '                    </div>' +
        '                    </div>' +
        '                    <div id="fpunkt2" class="fs fs-feature fs-kreis">' +
        '                    <div class="fs fs-feature fs-dropdown">    ' +
        '                    <div class="fs fs-feature fs-dropdown-content" id="ddblock2">' +
        '                       <a onclick=\'changeFeatureImageAndText("FCBpdc");\'>Parksensoren vorne und hinten</a>' +
        '                    </div>' +
        '                    </div>' +
        '                    </div>' +
        '                    <div id="fpunkt3" class="fs fs-feature fs-kreis">' +
        '                    <div class="fs fs-feature fs-dropdown">    ' +
        '                    <div class="fs fs-feature fs-dropdown-content" id="ddblock3">' +
        '                       <a onclick=\'changeFeatureImageAndText("FCBrkamera");\'>Rückfahrkamera</a>' +
        '                       <a onclick=\'changeFeatureImageAndText("FCBpdc");\'>Parksensoren vorne und hinten</a>' +
        '                    </div>' +
        '                    </div>' +
        '                    </div>' +
        '                </div>' +
        '' +
        '       <img id="bg-change" class="fs fs-feature fs-feature-wrapper-image" src="' + IMAGEPATH + 'featureView/X1/w.jpg">' +
        '       <button id="featureViewTargetBtn" onclick=\'scroll2TargetForm("' + car + '","Playful","FeatureView_' + car + '");\' class="btn btn-info fs fs-feature fs-target-btn">Angebot anfragen</button>' +
        '       <button onclick="renderParallaxView()" class="btn btn-info fs fs-feature fs-back-btn">Zurück</button>' +
'               </div>' +
        '' +
        '        <div class="fs fs-feature fs-colorpanel" id="fs-colorpanel" data-track-content data-content-name="FeatureView'+car+'" data-content-piece="FeatureViewColorPanel">' +
        '            <a onclick=\'colorFeatureImageWithSource("bild1.jpg", "X1");\'><img id="fs-cpp1" class="fs fs-feature fs-cpp" src="' + IMAGEPATH + 'featureView/X1/color1.jpg" alt="Alpinweiss" ></a>' +
        '            <a onclick=\'colorFeatureImageWithSource("bild2.jpg", "X1");\'><img id="fs-cpp2" class="fs fs-feature fs-cpp" src="' + IMAGEPATH + 'featureView/X1/color2.jpg" alt="Estoril Blau Metallic"></a>' +
        '            <a onclick=\'colorFeatureImageWithSource("bild3.jpg", "X1");\'><img id="fs-cpp3" class="fs fs-feature fs-cpp" src="' + IMAGEPATH + 'featureView/X1/color3.jpg" alt="Mediterranblau Metallic"></a>' +
        '            <a onclick=\'colorFeatureImageWithSource("bild4.jpg", "X1");\'><img id="fs-cpp4" class="fs fs-feature fs-cpp" src="' + IMAGEPATH + 'featureView/X1/color4.jpg" alt="Mineralgrau Metallic"></a>' +
        '        </div>' +
        '' +
        '    </div>' +
        '    <div id="FCBWrapper" >' +
        '    <div id="FCBContainer" >' + //class="container"
        '    <div id="FeatureContentBlock" class="row no gutter" data-track-content data-content-name="FeatureView'+car+'" data-content-piece="FeatureViewFCB" >' +
        '        <h1 id="FeatureViewHeader">' + X1HEADER + '</h1>' +
        '        <p id="FeatureViewText">' + X1TEXT + '</p>' +
        '    </br>' +
        '<p class="fs fs-disclaimer"> *Dieses Angebot ist gültig für alle BMW X1, BMW X2, BMW X3, BMWX4, BMW 3er Touring, BMW 3er Gran Turismo, BMW 4er Gran Coupé, BMW 5er Limousine, BMW 5er Touring, BMW 6er Gran Turismo und BMW X6 Neu- und Vorführwagen mit Kaufvertragsabschluss bis 31.03.2019 und Auslieferung bis 30.06.2019.</p>'+
        '' +
        '            <div id="FCBPictures" class="col-md-6"> ' +
        '                <div class="fs fs-feature fs-FCBinvisible" id="bFCBnavi">' +
        '                    <img class="fs fs-feature FCBB" src="' + IMAGEPATH + 'featureView/features/navi_plus.jpg" alt="Navigationssystem">' +
        '                </div>' +
        '                <div class="fs fs-feature fs-FCBinvisible" id="bFCBpdc">' +
        '                    <img class="fs fs-feature FCBB" src="' + IMAGEPATH + 'featureView/features/pdc.jpg" alt="Parksensoren">' +
        '                </div>' +
        '                <div class="fs fs-feature fs-FCBinvisible" id="bFCBrtti">' +
        '                    <img class="fs fs-feature FCBB" src="' + IMAGEPATH + 'featureView/features/rtti.jpg" alt="Echtzeit-Verkehrs&shy;informationen">' +
        '                </div>' +
        '                <div class="fs fs-feature fs-FCBinvisible" id="bFCBtelefonie">' +
        '                    <img class="fs fs-feature FCBB" src="' + IMAGEPATH + 'featureView/features/telefonie.jpg" alt="Telefonie">' +
        '                </div>' +
        '                <div class="fs fs-feature fs-FCBinvisible" id="bFCBrkamera">' +
        '                    <img class="fs fs-feature FCBB" src="' + IMAGEPATH + 'featureView/features/rueckfahrkamera.jpg" alt="Rückfahrkamera">' +
        '                </div>' +
        '            </div>' +
        '' +
        '            <div id="FCBTexts" class="col-md-6" > ' +
        '                ' +
        '                <div id="FCBnavi" class="fs fs-feature fs-FCBinvisible">' +
        '                    <h2>' + NAVIHEADER + '</h2>' +
        '                    <p>' + NAVITEXT + '</p></div>' +
        '                <div id="FCBtelefonie" class="fs fs-feature fs-FCBinvisible">' +
        '                    <h2>' + TELEFONIEHEADER + '</h2>' +
        '                    <p>' + TELEFONIETEXT + '</p></div>' +
        '                <div id="FCBrkamera" class="fs fs-feature fs-FCBinvisible">' +
        '                    <h2>' + KAMERAHEADER + '</h2>' +
        '                    <p>' + KAMERATEXT + '</p></div>' +
        '                <div id="FCBpdc" class="fs fs-feature fs-FCBinvisible">' +
        '                    <h2>' + PDCHEADER + '</h2>' +
        '                    <p>' + PDCTEXT + '</p></div>' +
        '                <div id="FCBrtti" class="fs fs-feature fs-FCBinvisible">' +
        '                    <h2>' + RTTIHEADER + '</h2>' +
        '                    <p>' + RTTITEXT + '</p></div>' +
        '            </div>' +
        '        </div>' +
        '        </div>' +
        '    </div>' +
        '</div>' +
        '</div>'
    ;
    return $resString;
}

//returns resource string for an HTML Block containing a modified bootstrap carousel
function createParallaxView() {

    let $resString =
        '<h2>Jetzt mit einem Preisvorteil von bis zu <strong>€ 4.300,-*.</strong> </h2></br>' +
        '<p>Individualisieren Sie sich den BMW X1, den BMW X2 oder den BMW X3. <br/> ' +
        ' Klicken Sie auf Ihr Wunschfahrzeug und entdecken Sie die Features die im Angebot inbegriffen sind.</p>' +
        '<br/>'+
        '<p class="fs fs-disclaimer"> *Dieses Angebot ist gültig für alle BMW X1, BMW X2, BMW X3, BMW X4, BMW 3er Touring, BMW 3er Gran Turismo, BMW 4er Gran Coupé, BMW 5er Limousine, BMW 5er Touring, BMW 6er Gran Turismo und BMW X6 Neu- und Vorführwagen mit Kaufvertragsabschluss bis 31.03.2019 und Auslieferung bis 30.06.2019.</p>'+
        '<div class="fs fs-parallaxView fs-clearfix" >' +
        '<div class="fs fs-parallaxView fs-box " id="parallaxX1" onclick=\'renderFeatureView("X1");\' data-track-content data-content-name="ParallaxX1" data-content-piece="parallaxX1" data-content-target="'+PUBLIC+'">' +
        '<div class="fs-left">' +
        '<div class="fs fs-parallaxView fs-parallax" onmouseleave="fsResetParalax(this)">' +
        '<img class="fs fs-parallaxView fs-imgX1 " style="z-index: 1;" src="' + IMAGEPATH + 'parallax/x1_p/x1_bg.png">' +
        '<img class="fs fs-parallaxView fs-imgX1 " style="z-index: 2;" src="' + IMAGEPATH + 'parallax/x1_p/x1_baueme.png">' +
        '<img class="fs fs-parallaxView fs-imgX1 " style="z-index: 3;" src="' + IMAGEPATH + 'parallax/x1_p/x1_auto.png">' +
        '</div>' +
        '</div>' +
        '<button class="btn btn-info fs fs-parallaxView fs-featureViewBtn" >BMW X1 individualisieren</button>' +
        '</div>' +
        '</div>' +
        '<div class="fs fs-parallaxView fs-clearfix">' +
        '<div class="fs fs-parallaxView fs-box " onclick=\'renderFeatureView("X2");\' data-track-content data-content-name="ParallaxX2" data-content-piece="parallaxX2" data-content-target="'+PUBLIC+'">' +
        '<div class="fs-right">' +
        '<div class="fs fs-parallaxView fs-parallax" id="parallaxX2" onmouseleave="fsResetParalax(this)">' +
        '<img class="fs fs-parallaxView fs-imgX2 " style="z-index: 1;" src="' + IMAGEPATH + 'parallax/x2_p/x2_hg.png">' +
        '<img class="fs fs-parallaxView fs-imgX2 " style="z-index: 2;" src="' + IMAGEPATH + 'parallax/x2_p/x2_baum.png">' +
        '<img class="fs fs-parallaxView fs-imgX2 " style="z-index: 3;" src="' + IMAGEPATH + 'parallax/x2_p/x2_auto.png">' +
        '</div>' +
        '</div>' +
        '<button class="btn btn-info fs fs-parallaxView fs-featureViewBtn" >BMW X2 individualisieren</button>' +
        '</div>' +
        '</div>' +
        '<div class="fs fs-parallaxView fs-clearfix">' +
        '<div class="fs fs-parallaxView fs-box " id="parallaxX3" onclick=\'renderFeatureView("X3");\' data-track-content data-content-name="ParallaxX3" data-content-piece="parallaxX3" data-content-target="'+PUBLIC+'">' +
        '<div class="fs-left">' +
        '<div class="fs fs-parallaxView fs-parallax " onmouseleave="fsResetParalax(this)">' +
        '<img class="fs fs-parallaxView fs-imgX3 " style="z-index: 1;" src="' + IMAGEPATH + 'parallax/x3_p/x3_bg.png">' +
        '<img class="fs fs-parallaxView fs-imgX3 " style="z-index: 2;" src="' + IMAGEPATH + 'parallax/x3_p/x3_berge.png">' +
        '<img class="fs fs-parallaxView fs-imgX3 " style="z-index: 3;" src="' + IMAGEPATH + 'parallax/x3_p/x3_auto.png">' +
        '</div>' +
        '</div>' +
        '<button class="btn btn-info fs fs-parallaxView fs-featureViewBtn" >BMW X3 individualisieren</button>' +
        '</div>' +
        '</div>'
        ;
    return $resString;
}

//returns resource string for an HTML Block containing a modified bootstrap carousel to showcase non spotlight cars
function createCarousel(variant) {
    let $resString =
        '<div class="fs fs-carousel fs-carousel-clearfix">' +
        '<div class="fs fs-carousel fs-carousel-box" id="fs-carouselbox-'+variant+'" data-track-content data-content-name="Carousel-'+variant+'" data-content-target="'+PUBLIC+'">' +

        '  <div id="nonSpotlightCars-'+variant+'" class="carousel slide fs fs-carousel fs-carousel-divder-img" data-ride="carousel" data-track-content data-content-name="Carousel-'+variant+'" data-content-piece="CarouselImgs">' +
        '    <!-- Indicators -->' +
        '    <ol class="carousel-indicators">' +
        '      <li data-target="#nonSpotlightCars-'+variant+'" data-slide-to="0" class="active"></li>' +
        '      <li data-target="#nonSpotlightCars-'+variant+'" data-slide-to="1"></li>' +
        '      <li data-target="#nonSpotlightCars-'+variant+'" data-slide-to="2"></li>' +
        '      <li data-target="#nonSpotlightCars-'+variant+'" data-slide-to="3"></li>' +
        '      <li data-target="#nonSpotlightCars-'+variant+'" data-slide-to="4"></li>' +
        '      <li data-target="#nonSpotlightCars-'+variant+'" data-slide-to="5"></li>' +
        '      <li data-target="#nonSpotlightCars-'+variant+'" data-slide-to="6"></li>' +
        '      <li data-target="#nonSpotlightCars-'+variant+'" data-slide-to="7"></li>' +
        '    </ol>' +
        '    <!-- Wrapper for slides -->' +
        '  <div class="carousel-inner">' +
        '    <div class="item active">' +
        '       <h2 class="fs fs-carousel fs-carousel-inv">NEUE ZIELE ERREICHEN.</h2>' +
        '       <h3 id="3T" class="fs fs-carousel fs-carousel-inv">Mit dem BMW 3er Touring.</h3>' +
        '       <p class="fs fs-carousel fs-carousel-inv">Mit seiner unwiderstehlichen Kombination aus sportlichem Design, Dynamik und hoher Funktionalität begeistert der BMW 3er Touring seit jeher. Mit Business Line überzeugt der BMW 3er Touring jetzt mit einem Preisvorteil von bis zu <strong> €&nbsp4.300,-* </strong> und inklusive Navigationssystem Professional, Telefonie mit Wireless Charging und vielem mehr.</p>' +
        '      <img class="fs fs-carousel fs-carousel-img" src="' + IMAGEPATH + 'bmw_3er_touring.jpg" alt="BMW 3er Touring">' +
        '    </div>' +
        '' +
        '      <div class="item">' +
        '       <h2 class="fs fs-carousel fs-carousel-inv">NEUE ZIELE ERREICHEN.</h2>' +
        '       <h3 id="3GT" class="fs fs-carousel fs-carousel-inv">Mit dem BMW 3er Gran Turismo.</h3>' +
        '       <p class="fs fs-carousel fs-carousel-inv">Ein vielseitiger Charakter voller Ideen und Ziele: Der BMW 3er Gran Turismo vereint sportliches Design, aufregende Dynamik und ein großzügiges Raumangebot in einem einzigartigen Fahrzeugkonzept. Mit Business Line überzeugt der BMW 3er Gran Turismo jetzt mit einem Preisvorteil von bis zu <strong> €&nbsp4.300,-* </strong> und inklusive Navigationssystem Professional, Telefonie mit Wireless Charging und vielem mehr.</p>' +
        '      <img class="fs fs-carousel fs-carousel-img" src="' + IMAGEPATH + 'bmw_3er_gt.jpg" alt="BMW 3er Gran Turismo">' +
        '      </div>' +
        '' +
        '      <div class="item">' +
        '       <h2 class="fs fs-carousel fs-carousel-inv">NEUE ZIELE ERREICHEN.</h2>' +
        '       <h3 id="X4" class="fs fs-carousel fs-carousel-inv">Mit dem BMW X4.</h3>' +
        '       <p class="fs fs-carousel fs-carousel-inv">Welche Herausforderungen auch immer auf Ihrem Weg liegen, der neue BMW X4 erwartet diese bereits mit Vorfreude. Sein progressives Design und die coupéhafte Linienführung machen seinen Tatendrang auf den ersten Blick deutlich. Mit Business Line überzeugt der BMW X4 jetzt mit einem Preisvorteil von bis zu € 4.300,-* und inklusive Navigationssystem Professional, Telefonie mit Wireless Charging und vielem mehr.</p>' +
        '      <img class="fs fs-carousel fs-carousel-img" src="' + IMAGEPATH + 'bmw_x4.jpg" alt="BMW X4">' +
        '      </div>' +
        '' +
        '      <div class="item">' +
        '       <h2 class="fs fs-carousel fs-carousel-inv">NEUE ZIELE ERREICHEN.</h2>' +
        '       <h3 id="4GC" class="fs fs-carousel fs-carousel-inv">Mit dem BMW 4er Gran Coupé.</h3>' +
        '       <p class="fs fs-carousel fs-carousel-inv">"Ein sportliches Coupé kombiniert mit einem eleganten Viertürer. Das Resultat ist vielversprechend: mehr Stil, mehr Raum, mehr Dynamik. Mit Business Line überzeugt das BMW BMW 4er Gran Coupé jetzt mit einem Preisvorteil von bis zu <strong> €&nbsp4.300,-* </strong> und inklusive Navigationssystem Professional, Telefonie mit Wireless Charging und vielem mehr.</p>' +
        '      <img class="fs fs-carousel fs-carousel-img" src="' + IMAGEPATH + 'bmw_4er_gran-coupe.jpg" alt="BMW 4er Gran Coupé">' +
        '      </div>' +
        '' +
        '      <div class="item">' +
        '       <h2 class="fs fs-carousel fs-carousel-inv" >NEUE ZIELE ERREICHEN.</h2>' +
        '       <h3 id="5L" class="fs fs-carousel fs-carousel-inv">Mit der BMW 5er Limousine.</h3>' +
        '       <p class="fs fs-carousel fs-carousel-inv">Der BMW 5er ist die Verkörperung der modernen Businesslimousine: Dynamisch und gleichzeitig elegant im Auftreten, ästhetisch und auf dem neuesten Stand der Technik. Mit Business Line überzeugt die BMW 5er Limousine jetzt mit einem Preisvorteil von bis zu <strong> €&nbsp4.300,-* </strong> und inklusive Navigationssystem Professional, Telefonie mit Wireless Charging und vielem mehr.</p>' +
        '      <img class="fs fs-carousel fs-carousel-img" src="' + IMAGEPATH + 'bmw_5er_limousine.jpg" alt="BMW 5er Limousine">' +
        '      </div>' +
        '' +
        '      <div class="item">' +
        '       <h2 class="fs fs-carousel fs-carousel-inv">NEUE ZIELE ERREICHEN.</h2>' +
        '       <h3 id="5T" class="fs fs-carousel fs-carousel-inv">Mit dem BMW 5er Touring.</h3>' +
        '       <p class="fs fs-carousel fs-carousel-inv">Herausragende Fahrdynamik, sportliches Design, maximaler Komfort – der BMW 5er Touring ist ein wahrer Meister seiner Klasse. Mit Business Line überzeugt der BMW 5er Touring jetzt mit einem Preisvorteil von bis zu <strong> €&nbsp4.300,-* </strong> und inklusive Navigationssystem Professional, Telefonie mit Wireless Charging und vielem mehr.</p>' +
        '      <img class="fs fs-carousel fs-carousel-img" src="' + IMAGEPATH + 'bmw_5er_touring.jpg" alt="BMW 5er Touring">' +
        '      </div>' +
        '' +
        '      <div class="item">' +
        '       <h2 class="fs fs-carousel fs-carousel-inv">NEUE ZIELE ERREICHEN.</h2>' +
        '       <h3 id="6GT" class="fs fs-carousel fs-carousel-inv">Mit dem BMW 6er Gran Turismo.</h3>' +
        '       <p class="fs fs-carousel fs-carousel-inv">Individualität, Ästhetik und außergewöhnlicher Fahrkomfort; mit diesen Eigenschaften überzeugt der BMW 6er Gran Turismo selbst die ansrpuchsvollsten Kunden. Seine souveränen Fahreigenschaften garantieren sportliche Dynamik – und auf Langstrecken höchsten Reisekomfort. Mit Business Line überzeugt der BMW 6er Gran Turismo jetzt mit einem Preisvorteil von bis zu <strong> €&nbsp4.300,-* </strong> und inklusive Navigationssystem Professional, Telefonie mit Wireless Charging und vielem mehr.</p>' +
        '      <img class="fs fs-carousel fs-carousel-img" src="' + IMAGEPATH + 'bmw_6er_gt.jpg" alt="BMW 6er Gran Turismo">' +
        '      </div>' +
        '' +
        '      <div class="item">' +
        '       <h2 class="fs fs-carousel fs-carousel-inv">NEUE ZIELE ERREICHEN.</h2>' +
        '       <h3 id="X6" class="fs fs-carousel fs-carousel-inv">Mit dem BMW X6.</h3>' +
        '       <p class="fs fs-carousel fs-carousel-inv">Kraftvoll und athletisch. Präsent wie ein X Modell, doch zugleich sportlich wie ein Coupé. Der BMW X6 sprengt alle Grenzen. Mit Business Line überzeugt der BMW X6 jetzt mit einem Preisvorteil von bis zu <strong> €&nbsp4.300,-* </strong> und inklusive Navigationssystem Professional, Telefonie mit Wireless Charging und vielem mehr.</p>' +
        '      <img class="fs fs-carousel fs-carousel-img" src="' + IMAGEPATH + 'bmw_x6.jpg" alt="BMW X6">' +
        '      </div>' +
        '    </div>' +
        '' +
        '    <!-- Left and right controls -->' +
        '    <a class="left carousel-control" href="#nonSpotlightCars-'+variant+'" data-slide="prev">' +
        '      <span class="glyphicon glyphicon-chevron-left"></span>' +
        '      <span class="sr-only">Previous</span>' +
        '    </a>' +
        '    <a class="right carousel-control" href="#nonSpotlightCars-'+variant+'" data-slide="next">' +
        '      <span class="glyphicon glyphicon-chevron-right"></span>' +
        '      <span class="sr-only">Next</span>' +
        '    </a>' +
        '  </div>' +
        '</div>' +
        '</div>' +
        '' + //3er 3er gt 5er limo 5er touring x6
        '<div id="nonSpotlightCarsTextBox-'+variant+'" class="fs fs-carousel fs-carousel-divder-text" data-track-content data-content-name="Carousel-'+variant+'" data-content-piece="CarouselTextBox">' +
        '   <div class="fs fs-carousel fs-carousel-text">' +
        '      <h2 id="nonSpotlightCarsHeader-'+variant+'">NEUE ZIELE ERREICHEN.</h2>' +
        '      <h3 id="nonSpotlightCarsSubHeader-'+variant+'">Mit dem BMW 3er Touring.</h3>' +
        '      <p id="nonSpotlightCarsBody-'+variant+'">Mit seiner unwiderstehlichen Kombination aus sportlichem Design, Dynamik und hoher Funktionalität begeistert der BMW 3er Touring seit jeher. Mit Business Line überzeugt der BMW 3er Touring jetzt mit einem Preisvorteil von bis zu <strong> €&nbsp4.300,-* </strong> und inklusive Navigationssystem Professional, Telefonie mit Wireless Charging und vielem mehr.</p>' +
        '       <div id="nonSpotlightCarsBtnWrapper-'+variant+'">' +
        '       <button id="nonSpotlightCarsBtn3T-'+variant+'" onclick=\'scroll2TargetForm("3T","'+ variant +'","Carousel_3T");\' class="btn btn-info fs fs-carousel fs-btn ">Angebot anfragen</button>' +
        '       <button id="nonSpotlightCarsBtn3GT-'+variant+'" onclick=\'scroll2TargetForm("3GT","'+ variant +'","Carousel_3GT");\' class="btn btn-info fs fs-carousel fs-btn fs-carousel-inv">Angebot anfragen</button>' +
        '       <button id="nonSpotlightCarsBtnX4-'+variant+'" onclick=\'scroll2TargetForm("X4","'+ variant +'","Carousel_X4");\' class="btn btn-info fs fs-carousel fs-btn fs-carousel-inv">Angebot anfragen</button>' +
        '       <button id="nonSpotlightCarsBtn4GC-'+variant+'" onclick=\'scroll2TargetForm("4GC","'+ variant +'","Carousel_4GC");\' class="btn btn-info fs fs-carousel fs-btn fs-carousel-inv">Angebot anfragen</button>' +
        '       <button id="nonSpotlightCarsBtn5L-'+variant+'" onclick=\'scroll2TargetForm("5L","'+ variant +'","Carousel_5L");\' class="btn btn-info fs fs-carousel fs-btn fs-carousel-inv">Angebot anfragen</button>' +
        '       <button id="nonSpotlightCarsBtn5T-'+variant+'" onclick=\'scroll2TargetForm("5T","'+ variant +'","Carousel_5T");\' class="btn btn-info fs fs-carousel fs-btn fs-carousel-inv">Angebot anfragen</button>' +
        '       <button id="nonSpotlightCarsBtn6GT-'+variant+'" onclick=\'scroll2TargetForm("6GT","'+ variant +'","Carousel_6GT");\' class="btn btn-info fs fs-carousel fs-btn fs-carousel-inv">Angebot anfragen</button>' +
        '       <button id="nonSpotlightCarsBtnX6-'+variant+'" onclick=\'scroll2TargetForm("X6","'+ variant +'","Carousel_X6");\' class="btn btn-info fs fs-carousel fs-btn fs-carousel-inv">Angebot anfragen</button>' +
        '       </div>' +
        '   </div>' +
        '   </br>' +
        '   <p class="fs fs-disclaimer"> *Dieses Angebot ist gültig für alle BMW X1, BMW X2, BMW X3, BMW X4, BMW 3er Touring, BMW 3er Gran Turismo, BMW 4er Gran Coupé, BMW 5er Limousine, BMW 5er Touring, BMW 6er Gran Turismo und BMW X6 Neu- und Vorführwagen mit Kaufvertragsabschluss bis 31.03.2019 und Auslieferung bis 30.06.2019.</p>'+

        '</div>'        
        ;
    return $resString;
}

//returns resource string for an HTML Block containing X1 features details.
function createAcc1() {
    let resString =
        '<div class="fs fs-acc fs-acc-clearfix">' +
        '<div class="fs fs-acc fs-acc-box" data-track-content data-content-name="AccordionX1" data-content-target="'+PUBLIC+'">' +
        '    <div class="row no-gutters">' +
        '      <div id="picturesX1" class="col-md-6 fs fs-acc fs-img-frame" data-track-content data-content-name="AccordionX1" data-content-piece="BilderX1">' +
        '          <img id="pictureX1" src="' + IMAGEPATH + 'accordion/bmw_x1.jpg" alt="features" width=100% >' +
        '          <div class="fs fs-acc fs-overlay" id="naviX1">' +
        '              <img class="fs fs-acc fs-acc-img" src="' + IMAGEPATH + 'accordion/features/navi.jpg" alt="Navigationssystem">' +
        '          </div>' +
        '          <div class="fs fs-acc fs-overlay" id="pdcX1">' +
        '              <img class="fs fs-acc fs-acc-img" src="' + IMAGEPATH + 'accordion/features/pdc.jpg" alt="Parksensoren">' +
        '          </div>' +
        '          <div class="fs fs-acc fs-overlay" id="rttiX1">' +
        '              <img class="fs fs-acc fs-acc-img" src="' + IMAGEPATH + 'accordion/features/rtti.jpg" alt="x1">' +
        '          </div>' +
        '          <div class="fs fs-acc fs-overlay" id="telefonieX1">' +
        '              <img class="fs fs-acc fs-acc-img"  src="' + IMAGEPATH + 'accordion/features/telefonie.jpg" alt="x1">' +
        '          </div>' +
        '          <div class="fs fs-acc fs-overlay" id="kameraX1">' +
        '              <img class="fs fs-acc fs-acc-img" src="' + IMAGEPATH + 'accordion/features/kamera.jpg" alt="x1">' +
        '          </div>' +
        '      </div>' +

        '      <div class="col-md-6" data-track-content data-content-name="AccordionX1" data-content-piece="TextX1">' +
        '        <div class="fs fs-acc fs-container-panel">' +
        '          <h2>NEUE ZIELE ERREICHEN.</h2>' +
        '          <h3>Mit dem BMW X1.</h3>' +
        '          <p>' + X1TEXT + '</p>' +
        '          <div class="panel-group" id="accordionX1">' +
        '            <div class="fs fs-acc fs-panel panel panel-default">' +
        '              <div class="fs fs-acc fs-panel-heading panel-heading">' +
        '                <h4 class="panel-title">' +
        '                  <a id="accX1" data-toggle="collapse" data-parent="#accordionX1" href="#collapse1X1" onclick=\'accChangeImg("naviX1", "picturesX1", "pictureX1");\' >Navigationssystem Plus mit 8,8" Display <i class="fs fs-acc fs-i-right"></i></a>' +
        '                </h4>' +
        '              </div>' +
        '              <div id="collapse1X1" class="panel-collapse collapse">' +
        '                <div class="fs fs-acc fs-panel-body"><p>' + NAVITEXT + '</p></div>' +
        '              </div>' +
        '            </div>' +
        '            <div class="fs fs-acc fs-panel panel panel-default">' +
        '              <div class="fs fs-acc fs-panel-heading panel-heading">' +
        '                <h4 class="panel-title">' +
        '                  <a data-toggle="collapse" data-parent="#accordionX1" href="#collapse2X1" onclick=\'accChangeImg("telefonieX1", "picturesX1", "pictureX1");\'>Telefonie mit Wireless Charging<i class="fs fs-acc fs-i-right"></i></a>' +
        '                </h4>' +
        '              </div>' +
        '              <div id="collapse2X1" class="panel-collapse collapse">' +
        '                <div class="fs fs-acc fs-panel-body"><p>' + TELEFONIETEXT + '</p></div>' +
        '              </div>' +
        '            </div>' +
        '            <div class="fs fs-acc fs-panel panel panel-default">' +
        '              <div class="fs fs-acc fs-panel-heading panel-heading">' +
        '                <h4 class="panel-title">' +
        '                  <a data-toggle="collapse" data-parent="#accordionX1" href="#collapse3X1" onclick=\'accChangeImg("kameraX1", "picturesX1", "pictureX1");\'>Rückfahrkamera<i class="fs fs-acc fs-i-right"></i></a>' +
        '                </h4>' +
        '              </div>' +
        '              <div id="collapse3X1" class="panel-collapse collapse">' +
        '                <div class="fs fs-acc fs-panel-body"><p>' + KAMERATEXT + '</p></div>' +
        '              </div>' +
        '            </div>' +
        '            <div class="fs fs-acc fs-panel panel panel-default">' +
        '                <div class="fs fs-acc fs-panel-heading panel-heading">' +
        '                  <h4 class="panel-title">' +
        '                    <a data-toggle="collapse" data-parent="#accordionX1" href="#collapse4X1" onclick=\'accChangeImg("pdcX1", "picturesX1", "pictureX1");\'>Park Distance Control vorne und hinten<i class="fs fs-acc fs-i-right"></i></a>' +
        '                  </h4>' +
        '                </div>' +
        '                <div id="collapse4X1" class="panel-collapse collapse">' +
        '                   <div class="fs fs-acc fs-panel-body"><p>' + PDCTEXT + '</p></div>' +
        '                </div>' +
        '              </div>' +
        '              <div class="fs fs-acc fs-panel panel panel-default">' +
        '                <div class="fs fs-acc fs-panel-heading panel-heading">' +
        '                  <h4 class="panel-title">' +
        '                    <a data-toggle="collapse" data-parent="#accordionX1" href="#collapse5X1" onclick=\'accChangeImg("rttiX1", "picturesX1", "pictureX1");\'>Real Time Traffic Information<i class="fs fs-acc fs-i-right"></i></a>' +
        '                  </h4>' +
        '                </div>' +
        '                <div id="collapse5X1" class="panel-collapse collapse">' +
        '                   <div class="fs fs-acc fs-panel-body"><p>' + RTTITEXT + '</p></div>' +
        '                </div>' +
        '              </div>' +
        '           </div> ' +
        '               </br> ' +
        '               <button onclick=\'scroll2TargetForm("X1","Rational","Accordion_X1");\' class="btn btn-info fs fs-acc fs-btn">Angebot anfragen</button>' +
        '               </br> ' +
        '               </br> ' +
        '               <p class="fs fs-disclaimer"> *Dieses Angebot ist gültig für alle BMW X1 Neu- und Vorführwagen mit Kaufvertragsabschluss bis 31.03.2019 und Auslieferung bis 30.06.2019.</p>'+
        '       </div>' +
        '       </div>' +
        '   </div>' +
        '</div>' +
        '</div>'
    ;
    return resString;
}

//returns resource string for an HTML Block containing X2 features details.
function createAcc2() {
    let resString =
        '<div class="fs fs-acc fs-acc-clearfix">' +
        '<div class="fs fs-acc fs-acc-box" data-track-content data-content-name="AccordionX2" data-content-target="'+PUBLIC+'">' +
        '    <div class="row no-gutters">' +
        '      <div id="picturesX2" class="col-md-6 fs fs-acc fs-img-frame" data-track-content data-content-name="AccordionX2" data-content-piece="BilderX2">' +
        '          <img id="pictureX2" src="' + IMAGEPATH + 'accordion/bmw_x2.jpg" alt="features" width=100% >' +
        '          <div class="fs fs-acc fs-overlay" id="naviX2">' +
        '              <img class="fs fs-acc fs-acc-img" src="' + IMAGEPATH + 'accordion/features/navi.jpg" alt="Navigationssystem">' +
        '          </div>' +
        '          <div class="fs fs-acc fs-overlay" id="pdcX2">' +
        '              <img class="fs fs-acc fs-acc-img" src="' + IMAGEPATH + 'accordion/features/pdc.jpg" alt="Parksensoren">' +
        '          </div>' +
        '          <div class="fs fs-acc fs-overlay" id="rttiX2">' +
        '              <img class="fs fs-acc fs-acc-img" src="' + IMAGEPATH + 'accordion/features/rtti.jpg" alt="x1">' +
        '          </div>' +
        '          <div class="fs fs-acc fs-overlay" id="telefonieX2">' +
        '              <img class="fs fs-acc fs-acc-img"  src="' + IMAGEPATH + 'accordion/features/telefonie.jpg" alt="x1">' +
        '          </div>' +
        '          <div class="fs fs-acc fs-overlay" id="kameraX2">' +
        '              <img class="fs fs-acc fs-acc-img" src="' + IMAGEPATH + 'accordion/features/kamera.jpg" alt="x1">' +
        '          </div>' +
        '      </div>' +

        '      <div class="col-md-6" data-track-content data-content-name="AccordionX2" data-content-piece="TextX2">' +
        '        <div class="fs fs-acc fs-container-panel">' +
        '          <h2>NEUE ZIELE ERREICHEN.</h2>' +
        '          <h3>Mit dem ersten BMW X2.</h3>' +
        '          <p>' + X2TEXT + '</p>' +
        '          <div class="panel-group" id="accordionX2">' +
        '            <div class="fs fs-acc fs-panel panel panel-default">' +
        '              <div class="fs fs-acc fs-panel-heading panel-heading">' +
        '                <h4 class="panel-title">' +
        '                  <a id="accX2" data-toggle="collapse" data-parent="#accordionX1" href="#collapse1X2" onclick=\'accChangeImg("naviX2", "picturesX2", "pictureX2");\' >Navigationssystem Plus mit 8,8" Display <i class="fs fs-acc fs-i-right"></i></a>' +
        '                </h4>' +
        '              </div>' +
        '              <div id="collapse1X2" class="panel-collapse collapse">' +
        '                <div class="fs fs-acc fs-panel-body"><p>' + NAVITEXT + '</p></div>' +
        '              </div>' +
        '            </div>' +
        '            <div class="fs fs-acc fs-panel panel panel-default">' +
        '              <div class="fs fs-acc fs-panel-heading panel-heading">' +
        '                <h4 class="panel-title">' +
        '                  <a data-toggle="collapse" data-parent="#accordionX2" href="#collapse2X2" onclick=\'accChangeImg("telefonieX2", "picturesX2", "pictureX2");\'>Telefonie mit Wireless Charging<i class="fs fs-acc fs-i-right"></i></a>' +
        '                </h4>' +
        '              </div>' +
        '              <div id="collapse2X2" class="panel-collapse collapse">' +
        '                    <div class="fs fs-acc fs-panel-body"><p>' + TELEFONIETEXT + '</p></div>' +
        '              </div>' +
        '            </div>' +
        '            <div class="fs fs-acc fs-panel panel panel-default">' +
        '              <div class="fs fs-acc fs-panel-heading panel-heading">' +
        '                <h4 class="panel-title">' +
        '                  <a data-toggle="collapse" data-parent="#accordionX2" href="#collapse3X2" onclick=\'accChangeImg("kameraX2", "picturesX2", "pictureX2");\'>Rückfahrkamera<i class="fs fs-acc fs-i-right"></i></a>' +
        '                </h4>' +
        '              </div>' +
        '              <div id="collapse3X2" class="panel-collapse collapse">' +
        '                       <div class="fs fs-acc fs-panel-body"><p>' + KAMERATEXT + '</p></div>' +
        '              </div>' +
        '            </div>' +
        '            <div class="fs fs-acc fs-panel panel panel-default">' +
        '                <div class="fs fs-acc fs-panel-heading panel-heading">' +
        '                  <h4 class="panel-title">' +
        '                    <a data-toggle="collapse" data-parent="#accordionX2" href="#collapse4X2" onclick=\'accChangeImg("pdcX2", "picturesX2", "pictureX2");\'>Park Distance Control vorne und hinten<i class="fs fs-acc fs-i-right"></i></a>' +
        '                  </h4>' +
        '                </div>' +
        '                <div id="collapse4X2" class="panel-collapse collapse">' +
        '                       <div class="fs fs-acc fs-panel-body"><p>' + PDCTEXT + '</p></div>' +
        '                </div>' +
        '              </div>' +
        '              <div class="fs fs-acc fs-panel panel panel-default">' +
        '                <div class="fs fs-acc fs-panel-heading panel-heading">' +
        '                  <h4 class="panel-title">' +
        '                    <a data-toggle="collapse" data-parent="#accordionX2" href="#collapse5X2" onclick=\'accChangeImg("rttiX2", "picturesX2", "pictureX2");\'>Real Time Traffic Information<i class="fs fs-acc fs-i-right"></i></a>' +
        '                  </h4>' +
        '                </div>' +
        '                <div id="collapse5X2" class="panel-collapse collapse">' +
        '                       <div class="fs fs-acc fs-panel-body"><p>' + RTTITEXT + '</p></div>' +
        '                </div>' +
        '              </div>' +
        '           </div> ' +
        '               </br> ' +
        '                <button onclick=\'scroll2TargetForm("X2","Rational","Accordion_X2");\' class="btn btn-info fs fs-acc fs-btn">Angebot anfragen</button>' +
        '               </br> ' +
        '               </br> ' +
        '               <p class="fs fs-disclaimer"> *Dieses Angebot ist gültig für alle BMW X2 Neu- und Vorführwagen mit Kaufvertragsabschluss bis 31.03.2019 und Auslieferung bis 30.06.2019.</p>'+
        '       </div>' +
        '      </div>' +
        '   </div>' +
        '</div>' +
        '</div>'
    ;
    return resString;
}

//returns resource string for an HTML Block containing X3 features details.
function createAcc3() {
    let resString =
        '<div class="fs fs-acc fs-acc-clearfix">' +
        '<div class="fs fs-acc fs-acc-box" data-track-content data-content-name="AccordionX3" data-content-target="'+PUBLIC+'">' +
        '    <div class="row no-gutters">' +
        '      <div id="picturesX3" class="col-md-6 fs fs-acc fs-img-frame" data-track-content data-content-name="AccordionX3" data-content-piece="BillderX3">' +
        '          <img id="pictureX3" src="' + IMAGEPATH + 'accordion/bmw_x3.jpg" alt="features" width=100% >' +
        '          <div class="fs fs-acc fs-overlay" id="naviX3">' +
        '              <img class="fs fs-acc fs-acc-img" src="' + IMAGEPATH + 'accordion/features/navi.jpg" alt="Navigationssystem">' +
        '          </div>' +
        '          <div class="fs fs-acc fs-overlay" id="pdcX3">' +
        '              <img class="fs fs-acc fs-acc-img" src="' + IMAGEPATH + 'accordion/features/pdc.jpg" alt="Parksensoren">' +
        '          </div>' +
        '          <div class="fs fs-acc fs-overlay" id="rttiX3">' +
        '              <img class="fs fs-acc fs-acc-img" src="' + IMAGEPATH + 'accordion/features/rtti.jpg" alt="x1">' +
        '          </div>' +
        '          <div class="fs fs-acc fs-overlay" id="telefonieX3">' +
        '              <img class="fs fs-acc fs-acc-img"  src="' + IMAGEPATH + 'accordion/features/telefonie.jpg" alt="x1">' +
        '          </div>' +
        '          <div class="fs fs-acc fs-overlay" id="kameraX3">' +
        '              <img class="fs fs-acc fs-acc-img" src="' + IMAGEPATH + 'accordion/features/kamera.jpg" alt="x1">' +
        '          </div>' +
        '      </div>' +

        '      <div class="col-md-6" data-track-content data-content-name="AccordionX3" data-content-piece="TextX3">' +
        '        <div class="fs fs-acc fs-container-panel">' +
        '          <h2>NEUE ZIELE ERREICHEN.</h2>' +
        '          <h3>Mit dem BMW X3.</h3>' +
        '          <p>' + X3TEXT + '</p>' +
        '          <div class="panel-group" id="accordionX3">' +
        '            <div class="fs fs-acc fs-panel panel panel-default">' +
        '              <div class="fs fs-acc fs-panel-heading panel-heading">' +
        '                <h4 class="panel-title">' +
        '                  <a id="accX3" data-toggle="collapse" data-parent="#accordionX3" href="#collapse1X3" onclick=\'accChangeImg("naviX3", "picturesX3", "pictureX3");\' >Navigationssystem Plus mit 8,8" Display<i class="fs fs-acc fs-i-right"></i></a>' +
        '                </h4>' +
        '              </div>' +
        '              <div id="collapse1X3" class="panel-collapse collapse">' +
        '                       <div class="fs fs-acc fs-panel-body"><p>' + NAVITEXT + '</p></div>' +
        '              </div>' +
        '            </div>' +
        '            <div class="fs fs-acc fs-panel panel panel-default">' +
        '              <div class="fs fs-acc fs-panel-heading panel-heading">' +
        '                <h4 class="panel-title">' +
        '                  <a data-toggle="collapse" data-parent="#accordionX3" href="#collapse2X3" onclick=\'accChangeImg("telefonieX3", "picturesX3", "pictureX3");\'>Telefonie mit Wireless Charging<i class="fs fs-acc fs-i-right"></i></a>' +
        '                </h4>' +
        '              </div>' +
        '              <div id="collapse2X3" class="panel-collapse collapse">' +
        '                       <div class="fs fs-acc fs-panel-body"><p>' + TELEFONIETEXT + '</p></div>' +
        '              </div>' +
        '            </div>' +
        '            <div class="fs fs-acc fs-panel panel panel-default">' +
        '              <div class="fs fs-acc fs-panel-heading panel-heading">' +
        '                <h4 class="panel-title">' +
        '                  <a data-toggle="collapse" data-parent="#accordionX3" href="#collapse3X3" onclick=\'accChangeImg("kameraX3", "picturesX3", "pictureX3");\'>Rückfahrkamera<i class="fs fs-acc fs-i-right"></i></a>' +
        '                </h4>' +
        '              </div>' +
        '              <div id="collapse3X3" class="panel-collapse collapse">' +
        '                       <div class="fs fs-acc fs-panel-body"><p>' + KAMERATEXT + '</p></div>' +
        '              </div>' +
        '            </div>' +
        '            <div class="fs fs-acc fs-panel panel panel-default">' +
        '                <div class="fs fs-acc fs-panel-heading panel-heading">' +
        '                  <h4 class="panel-title">' +
        '                    <a data-toggle="collapse" data-parent="#accordionX3" href="#collapse4X3" onclick=\'accChangeImg("pdcX3", "picturesX3", "pictureX3");\'>Park Distance Control vorne und hinten<i class="fs fs-acc fs-i-right"></i></a>' +
        '                  </h4>' +
        '                </div>' +
        '                <div id="collapse4X3" class="panel-collapse collapse">' +
        '                       <div class="fs fs-acc fs-panel-body"><p>' + PDCTEXT + '</p></div>' +
        '                </div>' +
        '              </div>' +
        '              <div class="fs fs-acc fs-panel panel panel-default">' +
        '                <div class="fs fs-acc fs-panel-heading panel-heading">' +
        '                  <h4 class="panel-title">' +
        '                    <a data-toggle="collapse" data-parent="#accordionX3" href="#collapse5X3" onclick=\'accChangeImg("rttiX3", "picturesX3", "pictureX3");\'>Real Time Traffic Information<i class="fs fs-acc fs-i-right"></i></a>' +
        '                  </h4>' +
        '                </div>' +
        '                <div id="collapse5X3" class="panel-collapse collapse">' +
        '                       <div class="fs fs-acc fs-panel-body"><p>' + RTTITEXT + '</p></div>' +
        '                </div>' +
        '              </div>' +
        '           </div> ' +
        '               </br> ' +
        '                <button onclick=\'scroll2TargetForm("X3","Rational","Accordion_X3");\' class="btn btn-info fs fs-acc fs-btn">Angebot anfragen</button>' +
        '               </br> ' +
        '               </br> ' +
        '               <p class="fs fs-disclaimer"> *Dieses Angebot ist gültig für alle BMW X3 Neu- und Vorführwagen mit Kaufvertragsabschluss bis 31.03.2019 und Auslieferung bis 30.06.2019.</p>'+
        '       </div>' +
        '       </div>' +
        '   </div>' +
        '</div>' +
        '</div>'
    ;
    return resString;
}
