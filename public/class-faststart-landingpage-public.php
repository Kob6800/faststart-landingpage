<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://hoelblingdaniel.myportfolio.com/
 * @since      1.0.0
 *
 * @package    Faststart_Landingpage
 * @subpackage Faststart_Landingpage/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Faststart_Landingpage
 * @subpackage Faststart_Landingpage/public
 * @author     Daniel Hölbling <hoelbling.daniel@hotmail.com>
 */
class Faststart_Landingpage_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Faststart_Landingpage_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Faststart_Landingpage_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

        if ( ! is_page_template('bmw-templates/page-landingpage.tpl.php') ) {
            return;
        }
        wp_enqueue_style( $this->plugin_name.'-bs-style', plugin_dir_url( __FILE__ ) . 'resources/bootstrap/css/bootstrap.min.css', array(), $this->version, 'all' );
        wp_enqueue_style( $this->plugin_name.'-style', plugin_dir_url( __FILE__ ) . 'css/style.css', array(), $this->version, 'all' );
        global $post;
//        if( has_shortcode( $post->post_content, 'fs_initRational' ) || has_shortcode( $post->post_content, 'fs_initPlayful' ) ) {
//
//        }

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Faststart_Landingpage_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Faststart_Landingpage_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

        if ( ! is_page_template('bmw-templates/page-landingpage.tpl.php') ) {
            return;
        }
        wp_enqueue_script( $this->plugin_name .'-bootstrap', plugin_dir_url( __FILE__ ) . 'resources/bootstrap/js/bootstrap.min.js', '', $this->version, true );
        wp_enqueue_script( $this->plugin_name.'-script', plugin_dir_url( __FILE__ ) . 'js/faststartLandingPage.js', array( 'jquery' ), $this->version, true );
        wp_enqueue_script( $this->plugin_name.'-tracking', plugin_dir_url( __FILE__ ) . 'js/tracking.js', '', $this->version, true );
        wp_localize_script($this->plugin_name.'-script', 'faststartLandingPage', array(
            'pluginUrl' => plugin_dir_url( __FILE__ ),
        ));
//        global $post;
//        if( has_shortcode( $post->post_content, 'fs_initRational' ) || has_shortcode( $post->post_content, 'fs_initPlayful' ) ) {
//
//        }
	}

    public function fs_initRationalFunc($atts) {
        $output ='
        <div id="fs-wrapper-rational" class="fs fs-wrapper">
            <div id="fs-cache-rational" class="fs fs-cache"></div>
            <div id="fs-content-rational" class="fs fs-content"></div>
        </div>';
        return $output;
    }

    public function fs_initPlayfulFunc ($atts) {
        $output ='
        <div id="fs-wrapper-playful" class="fs fs-wrapper">
            <div id="fs-cache-playful" class="fs fs-cache"></div>
            <div id="fs-content-playful" class="fs fs-content"></div>
        </div>';
        return $output;
    }
}
