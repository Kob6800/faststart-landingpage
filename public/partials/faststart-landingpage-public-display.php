<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://hoelblingdaniel.myportfolio.com/
 * @since      1.0.0
 *
 * @package    Faststart_Landingpage
 * @subpackage Faststart_Landingpage/public/partials
 */
?>
'<div class="fs fs-carousel fs-carousel-clearfix">' +
    '<div class="fs fs-carousel fs-carousel-box">' +
        '<div id="nonSpotlightCarousel" class="carousel slide fs fs-carousel fs-carousel-divder"  data-ride="carousel">' +
            '  <!-- Indicators -->' +
            '  <ol class="carousel-indicators">' +
                '    <li data-target="#nonSpotlightCarousel" data-slide-to="0" class="active"></li>' +
                '    <li data-target="#nonSpotlightCarousel" data-slide-to="1"></li>' +
                '    <li data-target="#nonSpotlightCarousel" data-slide-to="2"></li>' +
                '  </ol>' +
            '  <!-- Wrapper for slides -->' +
            '  <div class="carousel-inner">' +
                '    <div class="item active">' +
                    '       <p class="fs fs-carousel fs-carousel-inv">Hello 1</p>' +
                    '       <p class="fs fs-carousel fs-carousel-inv">TODO TEXT</p>' +
                    '      <img src="' + IMAGEPATH + 'bmw_x3.jpg" alt="TODO">' +
                    '    </div>' +
                '    <div class=" item">' +
                    '       <p class="fs fs-carousel fs-carousel-inv">Hello 2</p>' +
                    '       <p class="fs fs-carousel fs-carousel-inv">TODO TEXT </p>' +
                    '      <img src="' + IMAGEPATH + 'bmw_x3.jpg" alt="TODO">' +
                    '    </div>' +
                '    <div class=" item">' +
                    '       <p class="fs fs-carousel fs-carousel-inv">Hello 3</p>' +
                    '       <p class="fs fs-carousel fs-carousel-inv">TODO TEXT 1</p>' +
                    '      <img src="' + IMAGEPATH + 'bmw_x3.jpg" alt="TODO">' +
                    '    </div>' +
                '  </div>' +
            '  <!-- Left and right controls -->' +
            '  <a class="left carousel-control" href="#nonSpotlightCarousel" data-slide="prev">' +
                '    <span class="glyphicon glyphicon-chevron-left"></span>' +
                '    <span class="sr-only">Previous</span>' +
                '  </a>' +
            '  <a class="right carousel-control" href="#nonSpotlightCarousel" data-slide="next">' +
                '    <span class="glyphicon glyphicon-chevron-right"></span>' +
                '    <span class="sr-only">Next</span>' +
                '  </a>' +
            '  </div>' +

        '<div class="fs fs-carousel fs-carousel-divder">' +
            '<div class="fs fs-carousel fs-carousel-text">' +
                '<h2 id="nonSpotlightCarouselHeader">Header 1</h2>' +
                '<p id="nonSpotlightCarouselBody">lorem ipsum dol amet. lorem ipsum dol amet. lorem ipsum dol amet. lorem ipsum dol amet.</p>' +
            '</div>' +
        '</div>' +
    '</div>';
<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<!--<div id="fs-wrapper-rational" class="fs fs-wrapper">-->
<!--<div id="fs-cache-rational" class="fs fs-cache">-->
<!--<div id="fs-content-rational" class="fs fs-content">-->

<div class="fs fs-acc fs-container">
    <div class="row no-gutters">
        <div id="pictures" class="col-sm-6">
            <img id="picture" src="wp-content/plugins/faststart-landingpage/public/resources/images/accordion/bmw_x1.jpg" alt="features" width=100%>
            <div class="fs fs-acc fs-overlay" id="navi">
                <img class="fs fs-acc fs-img" src="wp-content/plugins/faststart-landingpage/public/resources/images/accordion/features/navi.jpg" alt="Navigationssystem">
            </div>
            <div class="fs fs-acc fs-overlay" id="pdc">
                <img class="fs fs-acc fs-img" src="wp-content/plugins/faststart-landingpage/public/resources/images/accordion/features/pdc.jpg" alt="Parksensoren">
            </div>
            <div class="fs fs-acc fs-overlay" id="rtti">
                <img class="fs fs-acc fs-img" src="wp-content/plugins/faststart-landingpage/public/resources/images/accordion/features/rtti.jpg" alt="x1">
            </div>
            <div class="fs fs-acc fs-overlay" id="telefonie">
                <img class="fs fs-acc fs-img" src="wp-content/plugins/faststart-landingpage/public/resources/images/accordion/features/telefonie.jpg" alt="x1">
            </div>
            <div class="fs fs-acc fs-overlay" id="kamera">
                <img class="fs fs-acc fs-img" src="wp-content/plugins/faststart-landingpage/public/resources/images/accordion/features/kamera.jpg" alt="x1">
            </div>
            <div class="fs fs-acc fs-overlay" id="sitzheizung">
                <img class="fs fs-acc fs-img" src="wp-content/plugins/faststart-landingpage/public/resources/images/accordion/features/sitzheizung.jpg" alt="x1">
            </div>
        </div>

        <div class="col-sm-6">
            <div class="fs fs-acc fs-container-panel">
                <h2>Lassen Sie kein Ziel unerreicht.</h2>
                <p>Der BMW X1 ist der perfekte Begleiter für alle, die jeden Tag neue Herausforderungen suchen.
                    Mit Business Line überzeugt der BMW X1 jetzt mit einem Preisvorteil von bis zu <strong> €
                        4.300,-* </strong> und innovativen Features:</p>

                <div class="panel-group" id="accordion">
                    <div class="fs fs-acc fs-panel panel panel-default">
                        <div class="fs fs-acc fs-panel-heading panel-heading">
                            <h4 class="panel-title">
                                <a id="acc1" data-toggle="collapse" data-parent="#accordion" href="#collapse1"
                                   onclick="accChangeImg('navi')">Navigationssystem Plus mit 8,8" Display</a>
                            </h4>
                        </div>
                        <div id="collapse1" class="panel-collapse collapse">
                            <div class="fs fs-acc fs-panel-body"><p>
                                    Das Navigationssystem Plus bietet Ihnen ein einfach zu bedienendes, optimal in Ihren
                                    BMW
                                    integriertes Navigationssystem inklusive iDrive Bedienkonzept.
                                    Die Zielführung erfolgt im 8,8"\hochauflösendem Farbbildschirm und kann per
                                    <strong>Sprachbefehl</strong> gesteuert werden. Zusätzlich werden Ihnen relevante
                                    Infos
                                    über das <strong>BMW Head-Up Display</strong> in die Windschutzscheibe projiziert.
                                </p></div>
                        </div>
                    </div>

                    <div class="fs fs-acc fs-panel panel panel-default">
                        <div class="fs fs-acc fs-panel-heading panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse2"
                                   onclick="accChangeImg('telefonie');">Telefonie mit Wireless Charging</a>
                            </h4>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse">
                            <div class="fs fs-acc fs-panel-body"><p>
                                    Dank Telefonie mit Wireless Charging können Sie Ihr Smartphone via Bluetooth mit
                                    Ihrem Fahrzeug
                                    verbinden und gleichzeitig kabellos aufladen. Zusätzlich beinhaltet dieses Feature
                                    die <strong>WLAN Hotspot Vorbereitung</strong>.
                                </p></div>
                        </div>
                    </div>

                    <div class="fs fs-acc fs-panel panel panel-default">
                        <div class="fs fs-acc fs-panel-heading panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse3"
                                   onclick=accChangeImg('kamera')>Rückfahrkamera</a>
                            </h4>
                        </div>
                        <div id="collapse3" class="panel-collapse collapse">
                            <div class="fs fs-acc fs-panel-body"><p>
                                    Die Rückfahrkamera zeigt den Bereich hinter dem Fahrzeug während des Ein- und
                                    Ausparkens an.
                                    Optional wird der Fahrer durch zusätzliche Parkhilfslinien und Hindernismarkierungen
                                    unterstützt.
                                </p></div>
                        </div>
                    </div>

                    <div class="fs fs-acc fs-panel panel panel-default">
                        <div class="fs fs-acc fs-panel-heading panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse4"
                                   onclick="accChangeImg('pdc');">Park Distance Control vorne und hinten</a>
                            </h4>
                        </div>
                        <div id="collapse4" class="panel-collapse collapse">
                            <div class="fs fs-acc fs-panel-body"><p>
                                    Die Park Distance Control PDC vorne und hinten, erleichtert Ihnen das Einparken und
                                    Rangieren auf engstem Raum.
                                    Signaltöne sowie eine optische Abstandsanzeige im Control Display
                                    (Grün/Gelb/Rot) melden Ihnen eine Annäherung an Objekte hinter bzw. vor Ihrem
                                    Fahrzeug. .
                                </p></div>
                        </div>
                    </div>

                    <div class="fs fs-acc fs-panel panel panel-default">
                        <div class="fs fs-acc fs-panel-heading panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse5"
                                   onclick="accChangeImg('ritti');">Real Time Traffic Information</a>
                            </h4>
                        </div>
                        <div id="collapse5" class="panel-collapse collapse">
                            <div class="fs fs-acc fs-panel-body"><p>
                                    Real Time Traffic Information hat das aktuelle Verkehrsgeschehen immer für Sie im
                                    Blick.
                                    Zu Staus auf dem Weg erhalten Sie frühzeitig und automatisch Vorschläge zu
                                    zeitsparenden Ausweichrouten.
                                    Die Informationen über Probleme auf Ihrer
                                    geplanten Route werden dabei nahezu in <strong>Echtzeit</strong> erfasst und sind
                                    bis auf hundert Meter genau.
                                </p></div>
                        </div>
                    </div>

                    <div class="fs fs-acc fs-panel panel panel-default">
                        <div class="fs fs-acc fs-panel-heading panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse6"
                                   onclick="accChangeImg('sitzheizung');">Sitzheizung</a>
                            </h4>
                        </div>
                        <div id="collapse6" class="panel-collapse collapse">
                            <div class="fs fs-acc fs-panel-body"><p>
                                    Die BMW Sitzheizung für Fondsitze sorgt in kurzer Zeit für angenehme
                                    Wärme der Sitzoberfläche und steigert das Wohlbefinden. Die Temperatur ist über
                                    zwei Taster in der Mittelkonsole getrennt regelbar.
                                </p></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--</div>-->
<!--</div>-->
<!--</div>-->