<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://hoelblingdaniel.myportfolio.com/
 * @since      1.0.0
 *
 * @package    Faststart_Landingpage
 * @subpackage Faststart_Landingpage/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
