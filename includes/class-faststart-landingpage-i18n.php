<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://hoelblingdaniel.myportfolio.com/
 * @since      1.0.0
 *
 * @package    Faststart_Landingpage
 * @subpackage Faststart_Landingpage/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Faststart_Landingpage
 * @subpackage Faststart_Landingpage/includes
 * @author     Daniel Hölbling <hoelbling.daniel@hotmail.com>
 */
class Faststart_Landingpage_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'faststart-landingpage',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
