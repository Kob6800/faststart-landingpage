<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://hoelblingdaniel.myportfolio.com/
 * @since      1.0.0
 *
 * @package    Faststart_Landingpage
 * @subpackage Faststart_Landingpage/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Faststart_Landingpage
 * @subpackage Faststart_Landingpage/includes
 * @author     Daniel Hölbling <hoelbling.daniel@hotmail.com>
 */
class Faststart_Landingpage_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
	    //TODO CLEANUP ?
//        wp_dequeue_script('fs-bootstrap-script' );
//        wp_deregister_script('fs-bootstrap-script');
//        wp_dequeue_script('fs-popper-script' );
//        wp_deregister_script('fs-popper-script');
//        wp_dequeue_script('fs-jquery-script' );
//        wp_deregister_script('fs-jquery-script');
//
//        wp_dequeue_style( 'fs-bs-style');
//        wp_dequeue_style( 'fs_adwerba');
    }
}
